/* eslint-disable camelcase */
'use strict'
const __importDefault = (this && this.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { default: mod }
}
Object.defineProperty(exports, '__esModule', { value: true })
const express_1 = __importDefault(require('express'))
const dotenv_1 = __importDefault(require('dotenv'))
// Configuration the .env file
// eslint-disable-next-line camelcase
dotenv_1.default.config()
// Create Express app
const app = (0, express_1.default)()
const port = process.env.port || 8000
// Define the first app route
app.get('/', (request, response) => {
  response.send({
    data: 'Goodbye, world'
  })
})
app.get('/hello', (request, response) => {
  response.send({
    data: `Hola, ${request.query.name}`
  })
})

// Execute APP and listen to port 8000
app.listen(port, () => console.log(`EXPRESS SERVER: Running at http://${port}`))
// # sourceMappingURL=index.js.map
