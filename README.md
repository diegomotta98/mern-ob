## Dependencies ##
*dotenv* 
```
sera utilizada para poder generar archivos .env (environments) que nos permitan proteger, gestionar y organizar puertos, URIs, etc. necesarios.
```

*express*:
```
Libreria de JS que nos facilitara el trabajo a la hora de interactuar con el servidor, crear y gestionar requests y responses.
```

## DevDependencies ##
```
Aqui instalamos todas las dependencias que son exclusivamente para el desarrollo de la node-app.

- Entre otras, instalamos eslint que nos va a permitir establecer reglas de cara a la sintaxis y clean code de nuestro proyecto.

- Jest es una biblioteca y marco de prueba. Nos permitira crear nuestras pruebas en cualquier marco creado por JS.

 - Nodemon es una dependencia que nos facilitara el desarrollo de nuestra app, quedando a la escucha de cualquier cambio y actualizando automaticamente sin necesidad de 're-run' nuestra app.

 - Webpack es un empaquetador de modulos que, justamente, nos crea un archivo que contenga lo necesario para que nuestra app pueda ejecutarse.

 - Concurrently es otra dependencia que nos permitira, en este caso, la ejecucion de multiples scripts al mismo tiempo.
```
## Scripts ##
- build: ejecutara "npx tsc" para compilar el codigo escrito en TypeScript.

- start: ejecutara el comando para iniciar la app.

- dev: activara la dependencia concurrently, ejecutara *npx tsc --watch* que activara el constante listening en el puerto correspondiente y por ultimo guardara el codigo traducido JS en la carpeta 'dist'.

- test: ejecutara nuestras pruebas en el marco Jest.

- serve:coverage : ejecutara, Jest mediante, nuestras pruebas creadas generando un reporte al final de la ejecucion.

## ENVIRONMENT VARIABLES ##
Se debera crear un archivo con el nombre .env y en el mismo añadir los siguiente:
    -PORT, BASE URL
