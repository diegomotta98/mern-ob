import express, { Express, Request, Response } from "express";
import dotenv from 'dotenv';

// Configuration the .env file
dotenv.config();

// Create Express app
const app = express();
const port: string |number = process.env.port || 8000;

// Define the first app route
app.get('/', (request: Request, response: Response) => {
    response.send({
        "data":"Goodbye, world"
    });
});

app.get('/hello', (request: Request, response: Response) => {
        response.send({
            "data":`Hola, ${request.query.name}`
        });
});

//Execute APP and listen to port 8000
app.listen(port, () => console.log(`EXPRESS SERVER: Running at http://${port}`));
